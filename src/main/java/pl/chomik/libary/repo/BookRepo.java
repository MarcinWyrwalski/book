package pl.chomik.libary.repo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import pl.chomik.libary.domian.Book;

public interface BookRepo extends CrudRepository<Book, Long> {
}

