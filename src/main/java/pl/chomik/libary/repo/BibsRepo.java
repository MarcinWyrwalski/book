package pl.chomik.libary.repo;

import org.springframework.data.repository.CrudRepository;
import pl.chomik.libary.domian.Bibs;

public interface BibsRepo extends CrudRepository<Bibs, Long> {
}
