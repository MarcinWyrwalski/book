package pl.chomik.libary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomelibaryApplication {

    public static void main(String[] args) {
        SpringApplication.run(HomelibaryApplication.class, args);
    }

}
