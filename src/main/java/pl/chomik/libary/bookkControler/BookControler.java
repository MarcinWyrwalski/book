package pl.chomik.libary.bookkControler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.chomik.libary.repo.BibsRepo;
import pl.chomik.libary.repo.BookRepo;

@Controller
public class BookControler {

    @Autowired
    private BookRepo bookRepo;

    @Autowired
    private BibsRepo bibsRepo;

    @RequestMapping(value = "/" )
    public String index() {
        return "Index";
    }
}
