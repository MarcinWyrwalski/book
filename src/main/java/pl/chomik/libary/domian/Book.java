package pl.chomik.libary.domian;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table
@JsonIgnoreProperties(ignoreUnknown = true)
public class Book {

    @NotNull
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    @Cascade(CascadeType.PERSIST)
    private Bibs[] bibs;

    public Book() {
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Bibs[] getBibs() {
        return bibs;
    }

    public void setBibs(Bibs[] bibs) {
        this.bibs = bibs;
    }
}
