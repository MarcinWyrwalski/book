package pl.chomik.libary.domian;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "LibaryBooks")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Bibs implements Serializable {


    private Long id;
    private String language;
    private String subject;
    private String isbnIssn;
    private String author;
    private String placeOfPublication;
    private String title;
    private String publisher;
    private String kind;
    private String domain;
    private String formOfWork;
    private String gerne;
    private String publicationYear;

    public Bibs() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotNull
    @Column(name = "bibsid")
    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getPlaceOfPublication() {
        return placeOfPublication;
    }

    public void setPlaceOfPublication(String placeOfPublication) {
        this.placeOfPublication = placeOfPublication;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(String publicationYear) {
        this.publicationYear = publicationYear;
    }

    public String getLanguageOfOriginal() {
        return languageOfOriginal;
    }

    public void setLanguageOfOriginal(String languageOfOriginal) {
        this.languageOfOriginal = languageOfOriginal;
    }

    private String languageOfOriginal;



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getIsbnIssn() {
        return isbnIssn;
    }

    public void setIsbnIssn(String isbnIssn) {
        this.isbnIssn = isbnIssn;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getGerne() {
        return gerne;
    }

    public void setGerne(String gerne) {
        this.gerne = gerne;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getFormOfWork() {
        return formOfWork;
    }

    public void setFormOfWork(String formOfWork) {
        this.formOfWork = formOfWork;
    }



    @Override
    public String toString() {
        return "Bibs{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", isbnIssn='" + isbnIssn + '\'' +
                ", language='" + language + '\'' +
                ", gerne='" + gerne + '\'' +
                ", publisher='" + publisher + '\'' +
                ", formOfWork='" + formOfWork + '\'' +
                ", Id="  +
                ", isbn='" +  '\'' +
                '}';
    }
}
